/*
** accessor.h for accessor in /home/le-mai_s/librairie/librairie_cpp/macro_cpp
**
** Made by Sébastien Le Maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Wed Jan 13 18:55:20 2016 Sébastien Le Maire
** Last update Wed Jan 13 22:58:15 2016 Sébastien Le Maire
*/

#ifndef ACCESSOR_H_
# define ACCESSOR_H_

/*
** These macros used to initialize default accessor in c++.
** For use its, you must call all variables that you use
** by "m_" follow by variable name. ex: m_name to name's accessor.
** More, you must to writting prototype in header file at the class. (see prototype)
** These macros take three parameters :
** 1) class's name.
** 2) type of our variable.
** 3) name of the variable without "m_", its use to give function's name too.
**
** Exemple : SET(Character, std::string, name)
** this implemented "void  Character::setname(std::string const &name);"
** and set attribute members "this->m_name" to the name value.
**
** Warning! These macros only use if you need default accessor.
** You must recode functions if you need to give specific attribute.
*/

/*
** Prototype : void	set[VARNAME]([TYPE] const &);
*/
# define SET(class, mytype, varname)				\
void		class::set##varname(mytype const & varname)	\
{								\
	this->m_##varname = varname;				\
}								\

/*
** Prototype : [TYPE]	const	&get[VARNAME](void) const;
*/
# define GET(class, mytype, varname)				\
mytype	const	&class::get##varname(void) const	    	\
{								\
  return (this->m_##varname);					\
}								\

/*
** Define both defaults accessors.
*/
# define GET_SET(class, mytype, varname)			\
  GET(class, mytype, varname)					\
  SET(class, mytype, varname)					\

#endif /* !ACCESSOR_H_ */
