#ifndef POPO_H_
# define POPO_H_

# include <string>

class	popo
{
  std::string 	m_name;
  int		m_age;
  std::string	m_profession;

 public:
  popo();
  ~popo();
  std::string const  	&getname(void) const;
  void			setname(std::string const &name);
  int const	 	&getage(void) const;
  void			setage(int const &age);
  std::string const  	&getprofession(void) const;
  void			setprofession(std::string const &name);
};

#endif /* !POPO_H_ */
