#include <stdlib.h>
#include <iostream>
#include "popo.h"

int		main(void)
{
  popo		p;

  p.setname("Seb");
  p.setage(19);
  p.setprofession("Etudiant à Epitech");
  std::cout << "je m'appelle " << p.getname() << " et j'ai "
	    << p.getage() << " ans et je suis " << p.getprofession()
	    << std::endl;
  return (EXIT_SUCCESS);
}
