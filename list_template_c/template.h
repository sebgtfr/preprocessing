/*
** template.h for template in /home/le-mai_s/librairie/librairie_C/libtemplate/include
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Mon Dec  7 19:02:12 2015 sebastien le-maire
** Last update Fri Apr 29 09:18:59 2016 Sébastien Le Maire
*/


#ifndef TEMPLATE_H_
# define TEMPLATE_H_

# include <stdio.h>
# include <stdlib.h>

# define DEFINE_LIST(type)							\
										\
  struct			s_list_##type;					\
										\
  typedef struct		s_elem_list_##type				\
  {										\
    type			data;						\
    struct s_elem_list_##type	*next;						\
  }				t_elem_list_##type;				\
										\
  typedef struct		s_fct_list_##type				\
  {										\
    unsigned int		(*size_list)(const struct s_list_##type *);	\
    int				(*push_back)(struct s_list_##type *, type);	\
    void			(*delete_list)(struct s_list_##type *);		\
    type			(*index)(struct s_list_##type *, unsigned int);	\
    void			(*pop_back)(struct s_list_##type *);		\
    int				(*push_front)(struct s_list_##type *, type);	\
    void			(*pop_front)(struct s_list_##type *);		\
    int				(*insert)(struct s_list_##type *, type,		\
					  unsigned int);			\
    void			(*erase)(struct s_list_##type *, unsigned int);	\
    void			(*clear)(struct s_list_##type *);		\
    void			(*set)(struct s_list_##type *, type,		\
				       unsigned int);				\
  }				t_fct_list_##type;				\
										\
  typedef struct		s_list_##type					\
  {										\
    t_elem_list_##type		*list;						\
      unsigned int		size;						\
      t_fct_list_##type		*fct;						\
  }				t_list_##type;					\
										\
  t_list_##type			*new_list_##type();				\
										\
  unsigned int			size_list_##type(const t_list_##type *list);	\
										\
  int				push_back_##type(t_list_##type *list,		\
						 type data);			\
										\
  void				delete_list_##type(t_list_##type *list);	\
										\
  type				index_##type(t_list_##type *list,		\
					     unsigned int index);		\
										\
  void				pop_back_##type(t_list_##type *list);		\
										\
  int				push_front_##type(t_list_##type *list,		\
						  type data);			\
										\
  void				pop_front_##type(t_list_##type *list);		\
										\
  int				insert_##type(t_list_##type *list, type data,	\
					      unsigned int index);		\
										\
  void				erase_##type(t_list_##type *list,		\
					       unsigned int index);		\
										\
  void				clear_##type(t_list_##type *list);		\
										\
  void				set_##type(t_list_##type *list, type data,	\
					   unsigned int index);			\
										\
  unsigned int			size_list_##type(const t_list_##type *list)	\
  {										\
    return (list->size);							\
  }										\
										\
  int				push_back_##type(t_list_##type *list,		\
						 type data)			\
  {										\
    t_elem_list_##type		*elem;						\
										\
      if (!(elem = malloc(sizeof(*elem))))					\
	return (EXIT_FAILURE);							\
      elem->data = data;							\
      elem->next = NULL;							\
      ++list->size;								\
      if (!(list->list))							\
	list->list = elem;							\
      else									\
	{									\
	  t_elem_list_##type	*last;						\
										\
	    last = list->list;							\
	    while (last->next)							\
	      last = last->next;						\
	    last->next = elem;							\
	}									\
      return (EXIT_SUCCESS);							\
  }										\
										\
  int				push_front_##type(t_list_##type *list,		\
						  type data)			\
  {										\
    t_elem_list_##type		*elem;						\
										\
      if (!(elem = malloc(sizeof(*elem))))					\
	return (EXIT_FAILURE);							\
      elem->data = data;							\
      elem->next = list->list;							\
      list->list = elem;							\
      ++list->size;								\
      return (EXIT_SUCCESS);							\
  }										\
										\
  void				pop_back_##type(t_list_##type *list)		\
  {										\
    t_elem_list_##type		*last;						\
										\
      if (list && list->list)	       						\
	{									\
	  if (!(list->list->next))						\
	    {									\
	      free(list->list);							\
	      list->list = NULL;						\
	    }									\
	  else									\
	    {									\
	      last = list->list;						\
	      while (last->next->next)						\
		last = last->next;						\
	      free(last->next);							\
	      last->next = NULL;						\
	    }									\
	  list->size -= (!list->size) ? 0 : 1;					\
	}									\
  }										\
										\
  void				pop_front_##type(t_list_##type *list)		\
  {										\
    t_elem_list_##type		*first;						\
										\
    if (list && list->list)						       	\
      {										\
	if (!(list->list->next))						\
	  {									\
	    free(list->list);							\
	    list->list = NULL;							\
	  }									\
	else									\
	  {									\
	    first = list->list->next;						\
	    free(list->list);							\
	    list->list = first;							\
	  }									\
	list->size -= (!list->size) ? 0 : 1;					\
      }										\
  }										\
										\
  int				insert_##type(t_list_##type *list, type data,	\
					      unsigned int index)		\
  {										\
    t_elem_list_##type		*elem;						\
      t_elem_list_##type	*last;						\
      unsigned int		i;						\
										\
      if (list)									\
	{									\
	  if ((!(list->list)) || (!(list->list->next)) || (!index))		\
	      return ((index) ? push_back_##type(list, data) :			\
		      push_front_##type(list, data));				\
	  else									\
	    {									\
	      if (!(elem = malloc(sizeof(*elem))))				\
		return (EXIT_FAILURE);						\
	      elem->data = data;						\
	      i = 0;								\
	      last = list->list;						\
	      while ((i < (index - 1)) && (last->next))				\
		{								\
		  last = last->next;						\
		  ++i;								\
		}								\
	      elem->next = last->next;						\
	      last->next = elem;						\
	      ++list->size;							\
	    }									\
	  return (EXIT_SUCCESS);						\
	}									\
      return (EXIT_FAILURE);							\
  }										\
										\
  void				erase_##type(t_list_##type *list,		\
					     unsigned int index)		\
  {										\
    t_elem_list_##type		*elem;						\
      t_elem_list_##type	*prev;						\
      unsigned int		i;						\
										\
      if (list && list->list)							\
	{									\
	  if (index && list->size > 1) 						\
	    {									\
	      i = 0;								\
	      elem = list->list;						\
	      prev = elem;							\
	      while ((i < index) && (elem->next))				\
		{								\
		  elem = elem->next;						\
		  ++i;								\
		}								\
	      while (prev != elem && prev->next != elem)      			\
		prev = prev->next;						\
	      prev->next = elem->next;						\
	      free(elem);							\
	      list->size -= (!list->size) ? 0 : 1;				\
	    }									\
	  else									\
	    pop_front_##type(list);						\
	}									\
  }										\
										\
  void			        clear_##type(t_list_##type *list)		\
  {										\
   t_elem_list_##type	*cpy;							\
										\
    if (list)									\
	while (list->list)							\
	  {									\
	    cpy = list->list;							\
	    list->list = list->list->next;					\
	    free(cpy);								\
	  }									\
    list->size = 0;								\
  }										\
										\
  void				set_##type(t_list_##type *list, type data,	\
					   unsigned int index)			\
  {										\
    t_elem_list_##type		*elem;						\
      unsigned int		i;						\
										\
      if (list && list->list)							\
	{									\
	  i = 0;								\
	  elem = list->list;							\
	  while ((i < index) && (elem->next))					\
	    {									\
	      ++i;								\
	      elem = elem->next;						\
	    }									\
	  elem->data = data;							\
	}									\
  }										\
										\
  void				delete_list_##type(t_list_##type *list)		\
  {										\
    t_elem_list_##type	*cpy;							\
										\
    if (list)									\
      {										\
	if (list->list)								\
	  {									\
	    while (list->list)							\
	      {									\
		cpy = list->list;						\
		list->list = list->list->next;					\
		free(cpy);							\
	      }									\
	  }									\
	free(list);								\
      }										\
  }										\
										\
										\
  type				index_##type(t_list_##type *list,		\
					     unsigned int index)		\
  {										\
    unsigned int		i;						\
    t_elem_list_##type		*elem;						\
										\
      i = 0;									\
      elem = list->list;							\
      while ((i < index) && (elem->next))					\
	{									\
	  elem = elem->next;							\
	  ++i;									\
	}									\
      return (elem->data);							\
  }										\
										\
  t_fct_list_##type		fct_list_##type =				\
    {										\
      &size_list_##type,							\
      &push_back_##type,							\
      &delete_list_##type,							\
      &index_##type,								\
      &pop_back_##type,								\
      &push_front_##type,							\
      &pop_front_##type,							\
      &insert_##type,								\
      &erase_##type,								\
      &clear_##type,								\
      &set_##type								\
    };										\
										\
  t_list_##type			*new_list_##type()				\
  {										\
    t_list_##type		*list;						\
										\
      if (!(list = malloc(sizeof(*list))))					\
	return (NULL);								\
      list->size = 0;								\
      list->list = NULL;							\
      list->fct = &fct_list_##type;						\
	return (list);								\
  }										\

/*
** Constructor and destructor of the list
*/
# define NEW_LIST(type)			new_list_##type()
# define DELETE_LIST(list)		list->fct->delete_list(list)

/*
** Methods of the list :
** size_list	-> get number of node.
** push_back	-> add node in ending of list.
** index	-> get elem at idx node of list.
** pop_back	-> delete node in ending of list.
** push_front	-> add node in beginnig of list.
** pop_front	-> delete node in ending of list.
** insert	-> add node in idx node of list.
** erase	-> delete node in idx node of list.
** set		-> change value in idx node of list.
*/
# define SIZE_LIST(list)		list->fct->size_list(list)
# define PUSH_BACK(list, data)		list->fct->push_back(list, data)
# define INDEX(list, idx)		list->fct->index(list, idx)
# define POP_BACK(list)			list->fct->pop_back(list)
# define PUSH_FRONT(list, data)		list->fct->push_front(list, data)
# define POP_FRONT(list)		list->fct->pop_front(list)
# define INSERT(list, data, idx)	list->fct->insert(list, data, idx)
# define ERASE(list, idx)		list->fct->erase(list, idx)
# define SET(list, data, idx)		list->fct->set(list, data, idx)

/*
** Type of the list
*/
# define LIST(type)			t_list_##type*


/*
** Error
*/
# define ERROR(msg, ...)							\
  (void)fprintf(stderr, __FILE__ ":%d:" msg "\n", __LINE__, __VA_ARGS__)	\

#endif /* !TEMPLATE_H_ */
