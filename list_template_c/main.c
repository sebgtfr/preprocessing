/*
** main.c for test in /home/le-mai_s/librairie/librairie_C/libtemplate/include
**
** Made by sebastien le-maire
** Login   <le-mai_s@epitech.net>
**
** Started on  Mon Dec  7 19:15:30 2015 sebastien le-maire
** Last update Wed Jan 27 12:20:13 2016 Sébastien Le Maire
*/

#include "template.h"

DEFINE_LIST(int)
DEFINE_LIST(char)

int		main(void)
{
  LIST(int)	list_int;
  LIST(char)	list_char;
  unsigned int	i;

  list_int = NEW_LIST(int);
  list_char = NEW_LIST(char);
  PUSH_BACK(list_int, 10);
  POP_BACK(list_int);
  PUSH_FRONT(list_int, 10);
  POP_FRONT(list_int);
  PUSH_BACK(list_int, 20);
  PUSH_BACK(list_int, 40);
  PUSH_BACK(list_int, 80);
  PUSH_BACK(list_int, 160);
  PUSH_BACK(list_int, 320);
  PUSH_BACK(list_int, 640);
  PUSH_BACK(list_int, 1024);
  PUSH_FRONT(list_int, 0);
  PUSH_BACK(list_char, 'i');
  PUSH_BACK(list_char, 'n');
  PUSH_BACK(list_char, 'd');
  PUSH_BACK(list_char, 'e');
  PUSH_BACK(list_char, 'x');
  POP_FRONT(list_int);
  INSERT(list_int, 100, 3);
  ERASE(list_int, 3);
  for (i = 0; i < SIZE_LIST(list_int); ++i)
    {
      (void)printf("%c%c%c%c%c %d = %d\n", INDEX(list_char, 0),
		   INDEX(list_char, 1), INDEX(list_char, 2), INDEX(list_char, 3),
		   INDEX(list_char, 4), i, INDEX(list_int, i));
    }
  SET(list_int, 2048, 6);
  (void)printf("AFTER SET : %c%c%c%c%c %d = %d\n", INDEX(list_char, 0),
	       INDEX(list_char, 1), INDEX(list_char, 2), INDEX(list_char, 3),
	       INDEX(list_char, 4), 6, INDEX(list_int, 6));
  (void)printf("\nsize of list_int = %d\nsize of size of list_char = %d\n",
	       SIZE_LIST(list_int), SIZE_LIST(list_char));
  DELETE_LIST(list_int);
  DELETE_LIST(list_char);
  return (0);
}
